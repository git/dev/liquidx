# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# From: http://bugs.gentoo.org/show_bug.cgi?id=117291

inherit eutils python

DESCRIPTION="A tool for recording Flash SWF movies from VNC sessions."
HOMEPAGE="http://www.unixuser.org/~euske/vnc2swf/pyvnc2swf.html"
SRC_URI="http://www.unixuser.org/~euske/vnc2swf/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="x11vnc"

DEPEND=">=dev-lang/python-2.3
	>=dev-python/pygame-1.6
	x11vnc? ( x11-misc/x11vnc )"

#pkg_setup() {
#	python_tkinter_exists || die "TCL/TK required. Python must be compiled with the tcltk USE flag."
#}

src_unpack() {
	unpack ${A}
	cd ${S}
	# Add /usr/lib/pyvnc2swf to the path so that the auxiliary Python files
	# are found.
	epatch ${FILESDIR}/${P}-gentoo.patch
}

src_compile() {
	for file in $(echo crippled_des.py html_templates.py image.py movie.py\
	mp3.py output.py rfb.py swf.py crippled_des.py	html_templates.py\
	image.py movie.py mp3.py output.py rfb.py swf.py)
	do
		einfo Compiling $file.
		python_mod_compile $file
	done
}

src_install() {
	insinto /usr/lib/pyvnc2swf
	doins crippled_des.py* image.py* mp3.py* rfb.py* html_templates.py* 
	doins movie.py* output.py* swf.py*
	newbin edit.py pyvnc2swf-edit
	newbin play.py pyvnc2swf-play
	newbin vnc2swf.py pyvnc2swf
	if use x11vnc;
	then
		# Only install the recordwin script if x11vnc is to be used.
		newbin recordwin.sh pyvnc2swf-recordwin
	fi
	dodoc README.txt
	dohtml *.html
}
