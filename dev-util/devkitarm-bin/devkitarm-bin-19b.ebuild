# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-util/devhelp/devhelp-0.12.ebuild,v 1.1 2006/08/01 09:09:34 leonardop Exp $

DESCRIPTION="DevKitPro for ARM"
HOMEPAGE="http://devkitpro.org/"
SRC_URI="mirror://sourceforge/devkitpro/devkitARM_r${PV}-linux.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-* x86"
IUSE=""

DEPEND=""
S=${WORKDIR}/devkitARM

src_compile() {
	return
}

src_install() {
	local INSTDIR=/opt/devkitARM
	cd ${S}
	dodir ${INSTDIR}
	tar -cvf - . | tar -xvf - -C ${D}${INSTDIR}

	dodir /etc/env.d
	echo "DEVKITPRO=${INSTDIR}"  > ${D}/etc/env.d/99devkitarm
	echo "DEVKITARM=${INSTDIR}" >> ${D}/etc/env.d/99devkitarm
	echo "PATH=${INSTDIR}/bin"  >> ${D}/etc/env.d/99devkitarm
 }
