# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-libs/libexif/libexif-0.6.12-r4.ebuild,v 1.8 2006/01/07 11:43:17 eradicator Exp $

inherit eutils

DESCRIPTION="A C++ implementation of the Levenberg-Marquardt non-linear regression"
HOMEPAGE="http://www.ics.forth.gr/~lourakis/levmar/"
SRC_URI="http://www.ics.forth.gr/~lourakis/levmar/${P}.tgz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="lapack"
DEPEND="lapack? ( dev-lib/f2c virtual/blas virtual/lapack)"

src_unpack() {
	unpack ${A}
	cd "${S}"
	sed -e "s:-O3:${CFLAGS}:" -e 's:^LAPACKLIBS_PATH.*$:LAPACKLIBS=/usr/lib:' \
		-i Makefile

	if ! use lapack; then
		sed -e 's:^LAPACKFLAG:#LAPACKFLAG:' \
			-e 's:^LAPACKLIBS:#LAPACKLIBS:' \
			-i Makefile
	fi
		
}

src_compile() {
	emake liblevmar.a || die
}

src_install() {
	dolib liblevmar.a
	insinto /usr/include
	doins lm.h
	dodoc README.txt LICENSE
}