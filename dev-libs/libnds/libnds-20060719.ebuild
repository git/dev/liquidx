# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-util/devhelp/devhelp-0.12.ebuild,v 1.1 2006/08/01 09:09:34 leonardop Exp $

DESCRIPTION="Nintendo DS Libraries for devkitPro ARM"
HOMEPAGE="http://devkitpro.org/"
SRC_URI="mirror://sourceforge/devkitpro/libnds-src-20060719.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="-* x86"
IUSE=""

DEPEND="dev-util/devkitarm-bin"
S=${WORKDIR}

src_compile() {
	cd ${S}
	export DEVKITARM=/opt/devkitARM
	export PATH=/opt/devkitARM/bin:${PATH}
	emake || die "make failed"
}

src_install() {
	local INSTDIR=/opt/devkitARM
	cd ${S}
	dodir ${INSTDIR}/libnds
	tar -cvf - . | tar -xvf - -C ${D}${INSTDIR}/libnds
 }
