# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-libs/libexif/libexif-0.6.12-r4.ebuild,v 1.8 2006/01/07 11:43:17 eradicator Exp $

inherit eutils

DESCRIPTION="Library for parsing, editing, and saving IPTC data in JPEG/TIFF."
HOMEPAGE="http://libiptcdata.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86"
IUSE="nls"

DEPEND="dev-util/pkgconfig"
RDEPEND=""


src_compile() {
	econf $(use_enable nls) || die
	emake || die
}

src_install() {
	make DESTDIR="${D}" install || die
	# Keep around old lib
	# preserve_old_lib /usr/$(get_libdir)/libexif.so.9
}

#pkg_postinst() {
# preserve_old_lib_notify /usr/$(get_libdir)/libexif.so.9
#}
