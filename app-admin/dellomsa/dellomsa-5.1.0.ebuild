# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Dell OpenManage Server for Linux"
HOMEPAGE="http://linux.dell.com/monitoring.shtml"
DEB_URI="dellomsa_${PV}-2_i386.deb"
SRC_URI="http://linux.dell.com/files/openmanage-contributions/debian/omsa5.1/${DEB_URI}"
LICENSE="Dell"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""
DEPEND="app-arch/deb2targz
   sys-libs/openipmi
   net-misc/rsync"

RDEPEND="${DEPEND}"
S="${WORKDIR}/${P}"

src_unpack() {
	cd "${WORKDIR}"
	cp ${DISTDIR}/${DEB_URI} ${WORKDIR}
	deb2targz ${DEB_URI}
	tar zxvf ${DEB_URI/.deb/.tar.gz}
}

src_compile() {
	return
}

src_install() {
	rsync -avr ${WORKDIR}/opt ${D}
	
	ln -s /opt/dell/srvadmin/ ${D}/etc/delloma.d
	keepdir /opt/dell/srvadmin/shared/.sharedipc
	keepdir /opt/dell/srvadmin/oma/log
	keepdir /opt/dell/srvadmin/iws/{tmp,logs,config,contexts}
	keepdir /opt/dell/srvadmin/hapi/.ipc

	
	newinitd ${FILESDIR}/init_d_dellomsa dellomsa
	newconfd ${FILESDIR}/conf_d_dellomsa dellomsa
	newenvd  ${FILESDIR}/env_d_dellomsa 80dellomsa

	insinto /etc/pam.d
	newins ${FILESDIR}/pam_d_omauth omauth

	insinto /etc
	doins ${FILESDIR}/omreg.cfg

	dodir /usr/bin
	for exe in omreport omconfig omhelp; do
		ln -s /opt/dell/srvadmin/oma/bin/${exe} ${D}/usr/bin/${exe}
	done
}

pkg_postinst() {
	elog "Once the servers have started, connect to https://localhost:1311/"
}