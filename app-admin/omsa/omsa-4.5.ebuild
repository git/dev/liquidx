# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
#
# author: Wouter Coppens <wouter.coppens@dedigate.com>
#
# thanks a lot to Pauli Borodulin <pauli.borodulin@uta.fi> for writing the
# openmanage stuff on Debian
#
inherit eutils rpm toolchain-funcs

DESCRIPTION="Dell Openmanage"
HOMEPAGE="http://www1.us.dell.com/content/topics/global.aspx/solutions/en/openmanage?c=us&cs=555&l=en&s=biz"
SRC_BASE="450-32-335_A00"
LIBSTDC="296-2.96-132.fc4"
EXEVERSION="5.3.0.4531"
SRC_PACK="OMI-SrvAdmin-Dell-Web-LX-${SRC_BASE}.tar.gz"
SRC_PACK_LIBSTDC="compat-libstdc++-${LIBSTDC}.i386.rpm"
SRC_URI="ftp://ftp.dell.com/sysman/${SRC_PACK} ftp://rpmfind.net/linux/0/fedora/core/4/i386/os/Fedora/RPMS/${SRC_PACK_LIBSTDC}"
RPM_INSTALL_PREFIX="/opt/dell/srvadmin"

LICENSE="Dell"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
RESTRICT=""

RDEPEND="virtual/libc"

# Procmail is needed, because the bios update depends on it
DEPEND="${RDEPEND}
    mail-filter/procmail 
	>=app-arch/rpm-3.0.6"

pkg_setup() {
	# Binary x86 package
	has_multilib_profile && ABI="x86"
}

src_unpack() {
	unpack ${SRC_PACK}
	for d in srvadmin-base add-diagnostics add-storageservices add-webserver
	do
		cd ${WORKDIR}/linux/custom/${d}
		for x in *.rpm
		do
			rpm_unpack ${WORKDIR}/linux/custom/${d}/${x}
			rm -f ${WORKDIR}/linux/custom/${d}/${x}
		done
	done
	# patching the drivers
	cd ${WORKDIR}/linux/custom/srvadmin-base/opt/dell/srvadmin
	epatch "${FILESDIR}"/${PVR}/${P}-hapi.patch

	# extract compat-libstdc
	cd ${WORKDIR}/linux/custom
	rpm_unpack ${DISTDIR}/${SRC_PACK_LIBSTDC}
}

src_compile() {
	einfo "Fixing permissions..."
	for d in srvadmin-base add-diagnostics add-storageservices add-webserver
	do
		chown -R root:0 ${WORKDIR}/linux/custom/${d} || die
	done
}

src_install() {
	# install all files
	for d in srvadmin-base add-diagnostics add-storageservices add-webserver
	do
		cd ${WORKDIR}/linux/custom/${d}/${RPM_INSTALL_PREFIX}
		find . -print | cpio -pmud --quiet ${D}/${RPM_INSTALL_PREFIX} 2> /dev/null
	done

	# install compat-libstdc
	dodir ${RPM_INSTALL_PREFIX}/lib
	cd ${WORKDIR}/linux/custom/usr
	find lib -print | cpio -pmud --quiet ${D}/${RPM_INSTALL_PREFIX} 2> /dev/null

	# openmanage environment
	insinto /etc/env.d
	doins ${FILESDIR}/${PVR}/80openmanage

	# init.d scripts
	insinto /etc/conf.d
	newexe ${FILESDIR}/${PVR}/confd-dellomsa-daemons dellomsa-daemons
	exeinto /etc/init.d
	newexe ${FILESDIR}/${PVR}/dellomsa-drivers dellomsa-drivers
	newexe ${FILESDIR}/${PVR}/dellomsa-daemons dellomsa-daemons

	# create symbolic link
	dodir /etc/sysconfig
	dosym ${RPM_INSTALL_PREFIX}/iws/bin/linux/omawsd.src /etc/sysconfig/omawsd.src
	dosym ${RPM_INSTALL_PREFIX}/oma/bin/omsad.src /etc/sysconfig/omsad.src
	dosym ${RPM_INSTALL_PREFIX} /etc/openmanage
	dosym ${RPM_INSTALL_PREFIX} /etc/delloma.d
	dosym ${RPM_INSTALL_PREFIX}/oma/bin/omconfig.sh /usr/bin/omconfig
	dosym ${RPM_INSTALL_PREFIX}/oma/bin/omreport.sh /usr/bin/omreport
	dosym ${RPM_INSTALL_PREFIX}/oma/bin/omhelp.sh /usr/bin/omhelp
	dosym ${RPM_INSTALL_PREFIX}/oma/bin/omexec.sh /usr/bin/omexec
	dosym ${RPM_INSTALL_PREFIX}/oldiags/bin/omdiag.sh /usr/bin/omdiag
	# Create links to pci.ids
	dodir /usr/share/hwdata
	dosym /usr/share/misc/pci.ids /usr/share/hwdata/pci.ids
	dosym /usr/share/hwdata/pci.ids /usr/share/pci.ids

	# install builddriver script
	exeinto ${RPM_INSTALL_PREFIX}/hapi/bin
	newexe ${FILESDIR}/${PVR}/builddriver.sh builddriver.sh
	insinto ${RPM_INSTALL_PREFIX}/hapi/bin
	doins ${FILESDIR}/${PVR}/dcddks-2.6.14.patch dcddks-2.6.14.patch
}

pkg_postinst() {
	einfo ""
	einfo "Execute the following command when you want to setup the initial	environment."
	einfo "emerge --config =${PF}"
	einfo ""
}

pkg_config() {

	# Following line is needed, otherwise it gives always the following 
	# error: UNINSTALL:  file is not found
	export OMIIGNORESYSID="Y"

	# TODO
	einfo "Please ignore warning about:"
	einfo "sh: /etc/init.d/dataeng: No such file or directory"
	einfo

	# srvadmin-omilcore
	FUNCS="${RPM_INSTALL_PREFIX}/omil/prereqcheck/Funcs.sh"
	# patch FUNCS
	sed -ie 's/root.root/root:root/' ${FUNCS}
	. ${FUNCS}
	CreateShadowFuncs "srvadmin-omilcore" "${RPM_INSTALL_PREFIX}" "${FUNCS}"

	# create the omreg.cfg file
	if [ -f "/etc/omreg.cfg" ]; then
		# if it exist, we'll delete it anyway and create a new one
		rm -f /etc/omreg.cfg
	fi
	touch /etc/omreg.cfg && chmod 664 /etc/omreg.cfg && chown root:root /etc/omreg.cfg

	#  Update omreg.cfg with latest values
	UpdateRegPair /etc/omreg.cfg "" "openmanage.funcs=${FUNCS}"
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" openmanage.omilcore.installpath= openmanage.sysidchkutlpath=omil/prereqcheck/sysreport openmanage.syslistfile=omil/prereqcheck/syslist.txt openmanage.8gsyslistfile=omil/prereqcheck/8gsyslist.txt openmanage.funcs=omil/prereqcheck/Funcs.sh
	UpdateRegPair /etc/omreg.cfg "" openmanage.archtype=32

	##########################################################################
	# Adding the prerequisite checks for OMILCORE
	# These checks will warn the user or fail if certain
	# conditions are not met
	############################################################################
	ExitIfBadSysId /etc/omreg.cfg openmanage.sysidchkutlpathopenmanage.syslistfile srvadmin-omilcore UNINSTALL

	#srvadmin-deng
	FUNCS=`grep -i "^openmanage.funcs=" /etc/omreg.cfg | sed 's#^[^=]*=##'`
	. ${FUNCS}
	# System ID check
	ExitIfBadSysId /etc/omreg.cfg openmanage.sysidchkutlpath openmanage.syslistfile srvadmin-deng
	# Remove any mutex files that might exist
	# We want to clean up any mutex files on an upgrade
	rm -f ${RPM_INSTALL_PREFIX}/shared/.ipc/.dcsupmtx*
	ExitOnRelocCheck /etc/omreg.cfg openmanage.omilcore.installpath	srvadmin-omilcore "${RPM_INSTALL_PREFIX}" /opt/dell/srvadmin srvadmin-deng
	CreateShadowFuncs "srvadmin-deng" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	# update omreg file with latest values
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" OMDataEngine.installpath=dataeng OMDataEngine.logpath=dataeng OMDataEngine.vardatapath=dataeng OMDataEngine.configtool=dataeng/bin/dcecfg32 suptlib.installpath=shared suptlib.vardatapath=shared
	if [ -n "${DISABLESNMPD}" ]; then
		UpdateRegPair /etc/omreg.cfg "" OMDataEngine.startsnmpd=false
	else
		UpdateRegPair /etc/omreg.cfg "" OMDataEngine.startsnmpd=true
	fi
	# INI file handling
	# Copy "dynamic" Ini files from "ini/new" dir to "ini" dir
	cp ${RPM_INSTALL_PREFIX}/dataeng/ini/new/*dy*.ini ${RPM_INSTALL_PREFIX}/dataeng/ini/
	chmod 664 ${RPM_INSTALL_PREFIX}/dataeng/ini/*dy*.ini 
	# create suptlib, ipc dir and files
	if [ ! -d ${RPM_INSTALL_PREFIX}/shared/.ipc ];
	then
		mkdir ${RPM_INSTALL_PREFIX}/shared/.ipc
		chmod 711 ${RPM_INSTALL_PREFIX}/shared/.ipc
	fi
	# IPC file
	if [ ! -f ${RPM_INSTALL_PREFIX}/shared/.ipc/.sharedipc ];
	then
		touch ${RPM_INSTALL_PREFIX}/shared/.ipc/.sharedipc
		chmod 666 ${RPM_INSTALL_PREFIX}/shared/.ipc/.sharedipc
	fi
	# IPC INI file
	if [ ! -f ${RPM_INSTALL_PREFIX}/shared/.ipc/.lxsuptIPCini ];
	then
		touch ${RPM_INSTALL_PREFIX}/shared/.ipc/.lxsuptIPCini
		chmod 660 ${RPM_INSTALL_PREFIX}/shared/.ipc/.lxsuptIPCini
	fi
	# Register Ndx
	LOC_DENG_CONFIGTOOL=${RPM_INSTALL_PREFIX}/dataeng/bin/dcecfg32
	if [ ! -z "${LOC_DENG_CONFIGTOOL}" ] && [ -x ${LOC_DENG_CONFIGTOOL} ];
	then
		${LOC_DENG_CONFIGTOOL} command=adddareg prefix=de product=OMDataEngine enable=true
		${LOC_DENG_CONFIGTOOL} command=enablesnmp >/dev/null
	fi
	# remove any omclean version file
	RemoveRegPair /etc/omversion.cfg dataengine.version

	#srvadmin-omacore
	FUNCS=`grep -i "^openmanage.funcs=" /etc/omreg.cfg | sed 's#^[^=]*=##'`
	. ${FUNCS}
	CreateShadowFuncs "srvadmin-omacore" "${RPM_INSTALL_PREFIX}" "${FUNCS}"

	# Update omreg.cfg with latest values
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" openmanage.omacore.installpath=
	#  /etc/pam.d authentication
	cp -f ${RPM_INSTALL_PREFIX}/omauth /etc/pam.d
	chmod 755 /etc/pam.d/omauth
	#  create the content for omsad.src
	echo >${RPM_INSTALL_PREFIX}/oma/bin/omsad.src
	echo "export PROGRAM_NAME='OM Common Services'"	>>${RPM_INSTALL_PREFIX}/oma/bin/omsad.src
	echo "export INSTALL_ROOT=${RPM_INSTALL_PREFIX}" >>${RPM_INSTALL_PREFIX}/oma/bin/omsad.src
	echo "export PROGRAM_PATH=\$INSTALL_ROOT/oma/bin" >>${RPM_INSTALL_PREFIX}/oma/bin/omsad.src
	echo "export PROGRAM_BIN=omsad" >>${RPM_INSTALL_PREFIX}/oma/bin/omsad.src
	echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:${RPM_INSTALL_PREFIX}/oma/bin/" >>${RPM_INSTALL_PREFIX}/oma/bin/omsad.src
	#  expand OpenManage install path in omprv32.ini
	FixPath /etc/omreg.cfg oma/ini/omprv32.ini openmanage.omacore.installpath srvadmin-omacore "${RPM_INSTALL_PREFIX}" replace_om_omacore_path
	#  CLI Registration
	OMINSTPATHVALUE=`GetRegVal /etc/omreg.cfg openmanage.omilcore.installpath`
	CLIREGUTIL=${OMINSTPATHVALUE}/omil/tools/cli_ini_modifier.sh
	$CLIREGUTIL ${OMINSTPATHVALUE}/omil/supportfiles/ini/omacore.ini add
	# Register cm snmp mib with the Data Engine
	if [ "i386" != "ia64" ]; then
		DCECFG=`GetRegVal /etc/omreg.cfg OMDataEngine.configtool`
		if [ ! -z "${DCECFG}" ] && [ -x ${DCECFG} ]; then
			CMMIB_CMD="command=addmpialias update=TRUE startuptype=automatic"
			CMMIB_DFUNC="dispatchfn=DELLCM32_SMMIBImplementerDispatch"
			CMMIB_DNAME="Inventory Collector SNMP MIB Implementor"
			${DCECFG} ${CMMIB_CMD}  aliasname="dellcm32" pathfname="${RPM_INSTALL_PREFIX}/oma/bin/libinvmib32.so" ${CMMIB_DFUNC} displayname="${CMMIB_DNAME}"
		fi
	fi

	# srvadmin-jre
	CreateShadowFuncs "srvadmin-jre" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	# There is no .bin file in 64 version
	umask 022
	${RPM_INSTALL_PREFIX}/jre/jre.sh  ${RPM_INSTALL_PREFIX}/jre
	FixPath /etc/omreg.cfg oma/ini/omprv32.ini openmanage.omacore.installpath srvadmin-jre "${RPM_INSTALL_PREFIX}" replace_om_jre_path
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" openmanage.jre.installpath=

	# srvadmin-odf
	CreateShadowFuncs "srvadmin-odf" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	# update omprvXX.ini file
	FixPath /etc/omreg.cfg oma/ini/omprv32.ini openmanage.omacore.installpath srvadmin-odf "${RPM_INSTALL_PREFIX}" replace_om_odf_path

	# srvadmin-hapi
	CreateShadowFuncs "srvadmin-hapi" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	# update omreg file with latest values
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" hapi.installpath=hapi hapi.vardatapath=hapi hapi.configtool=hapi/bin/dchcfg32
	# HAPI IPC dir setup
	LOC_HAPI_VARDATAIPC_PATH="${RPM_INSTALL_PREFIX}/hapi/.ipc"
	if [ ! -e ${LOC_HAPI_VARDATAIPC_PATH} ];
	then
		mkdir ${LOC_HAPI_VARDATAIPC_PATH}
	fi
	chmod 711 ${LOC_HAPI_VARDATAIPC_PATH}

	# srvadmin-isvc
	CreateShadowFuncs "srvadmin-isvc" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	# update omreg file with latest values
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" Instrumentation.installpath=omsa Instrumentation.logpath=omsa Instrumentation.vardatapath=omsa Instrumentation.configtool=omsa/bin/dcicfg32
	# Copy "dynamic" Ini files from "ini/new" dir to "ini" dir
	cp ${RPM_INSTALL_PREFIX}/omsa/ini/new/*dy*.ini ${RPM_INSTALL_PREFIX}/omsa/ini
	# Modify the permissions on the ini files
	chmod 664 ${RPM_INSTALL_PREFIX}/omsa/ini/*dy*.ini

	# Register Ndx info with the Data Engine.
	DCECFG=`GetRegVal /etc/omreg.cfg OMDataEngine.configtool`
	if [ ! -z "${DCECFG}" ] && [ -x ${DCECFG} ];
	then
		${DCECFG} command=adddareg prefix=is product=Instrumentation enable=true
		# common for all
		LOC_ISVC_BINPATH=${RPM_INSTALL_PREFIX}/omsa/bin
		# dc-isep
		CMD_ISEP="command=addepialias update=TRUE startuptype=automatic"
		DCISEP_DPFUNC=DCISEP32_SMEventImplementerDispatch
		DCISEP_DNAME="Instrumentation Service Event Plugin Implementer"
		${DCECFG} ${CMD_ISEP} aliasname=dcisep32 pathfname="${LOC_ISVC_BINPATH}/libdcisep32.soa" dispatchfn=${DCISEP_DPFUNC} displayname="${DCISEP_DNAME}"
		# dc-snis
		CMD_SNIS="command=addmpialias update=TRUE startuptype=automatic"
		DCSNIS_DPFUNC=DCSNIS32_SMMIBImplementerDispatch
		DCSNIS_DNAME="Instrumentation Service SNMP MIB Implementer"
		${DCECFG} ${CMD_SNIS} aliasname=dcsnis32 pathfname="${LOC_ISVC_BINPATH}/libdcsnis32.so"	dispatchfn=${DCSNIS_DPFUNC} displayname="${DCSNIS_DNAME}"
		# dphw-adpt
		CMD_ADPT="command=addpopalias update=TRUE startuptype=automatic"
		DCADPT_DPFUNC=DCADPT32_SMPopulatorDispatch
		DCADPT_DNAME="Adapter Data Populator"
		${DCECFG} ${CMD_ADPT} aliasname=dcadpt32 pathfname="${LOC_ISVC_BINPATH}/libdcadpt32.so"	dispatchfn=${DCADPT_DPFUNC} displayname="${DCADPT_DNAME}"
		# ESM2 data populator is only supported on 32-bit systems
		# dphw-esm2
		CMD_ESM2="command=addpopalias update=TRUE startuptype=automatic"
		DCESMP_DPFUNC=DCESMP32_SMPopulatorDispatch
		DCESMP_DNAME="ESM2 Data Populator"
		${DCECFG} ${CMD_ESM2} aliasname=dcesmp32 pathfname="${LOC_ISVC_BINPATH}/libdcesmp32.so"	dispatchfn=${DCESMP_DPFUNC} displayname="${DCESMP_DNAME}"
		# dphw-iemp
		CMD_IEMP="command=addpopalias update=TRUE startuptype=automatic"
		DCIEMP_DPFUNC=DCIEMP32_SMPopulatorDispatch
		DCIEMP_DNAME="IPMI Emergency Management Port Data Populator"
		${DCECFG} ${CMD_IEMP} aliasname=dciemp32 pathfname="${LOC_ISVC_BINPATH}/libdciemp32.so" dispatchfn=${DCIEMP_DPFUNC} displayname="${DCIEMP_DNAME}"
		# dphw-ienv
		CMD_IENV="command=addpopalias update=TRUE startuptype=automatic"
		DCIENV_DPFUNC=DCIENV32_SMPopulatorDispatch
		DCIENV_DNAME="IPMI Environmental Data Populator"
		${DCECFG} ${CMD_IENV} aliasname=dcienv32 pathfname="${LOC_ISVC_BINPATH}/libdcienv32.so" dispatchfn=${DCIENV_DPFUNC} displayname="${DCIENV_DNAME}"
		# dphw-ipmi09
		CMD_IPMIP9="command=addpopalias update=TRUE startuptype=automatic"
		DCIPMI9_DPFUNC=DCIPM932_SMPopulatorDispatch
		DCIPMI9_DNAME="IPMI 0.9 Data Populator"
		${DCECFG} ${CMD_IPMIP9} aliasname=dcipm932 pathfname="${LOC_ISVC_BINPATH}/libdcipm932.so" dispatchfn=${DCIPMI9_DPFUNC} displayname="${DCIPMI9_DNAME}"
		# dphw-ipmi10
		CMD_IPMIP10="command=addpopalias update=TRUE startuptype=automatic"
		DCIPMI10_DPFUNC=DCIP1032_SMPopulatorDispatch
		DCIPMI10_DNAME="IPMI 1.0 Data Populator"
		${DCECFG} ${CMD_IPMIP10} aliasname=dcip1032	pathfname="${LOC_ISVC_BINPATH}/libdcip1032.so" dispatchfn=${DCIPMI10_DPFUNC} displayname="${DCIPMI10_DNAME}"
		# dphw-sbp
		CMD_SBP="command=addpopalias update=TRUE startuptype=automatic"
		DCSBP_DPFUNC=DCSBPP32_SMPopulatorDispatch
		DCSBP_DNAME="SMBIOS Probe Data Populator"
		${DCECFG} ${CMD_SBP} aliasname=dcsbpp32	pathfname="${LOC_ISVC_BINPATH}/libdcsbpp32.so" dispatchfn=${DCSBP_DPFUNC}  displayname="${DCSBP_DNAME}"
		# TVM data populator is only supported on 32-bit systems
		# dphw-tvm
		CMD_TVM="command=addpopalias update=TRUE startuptype=automatic"
		DCTVM_DPFUNC=DCTVM32_SMPopulatorDispatch
		DCTVM_DNAME="TVM Data Populator"
		${DCECFG} ${CMD_TVM} aliasname=dctvm32 pathfname="${LOC_ISVC_BINPATH}/libdctvm32.so" dispatchfn=${DCTVM_DPFUNC}  displayname="${DCTVM_DNAME}"
		# dphw-wfm
		CMD_WFM="command=addpopalias update=TRUE startuptype=automatic"
		DCWFM_DPFUNC=DCWFM32_SMPopulatorDispatch
		DCWFM_DNAME="Wired for Management Data Populator"
		${DCECFG} ${CMD_WFM} aliasname=dcwfm32 pathfname="${LOC_ISVC_BINPATH}/libdcwfm32.so" dispatchfn=${DCWFM_DPFUNC}  displayname="${DCWFM_DNAME}"
		# dpmisc
		CMD_MISC="command=addpopalias update=TRUE startuptype=automatic"
		DCCOOP_DPFUNC=DCCOOP32_SMPopulatorDispatch
		DCCOOP_DNAME="Cost of Ownership Data Populator"
		DCLRA_DPFUNC=DCLRA32_SMPopulatorDispatch
		DCLRA_DNAME="Local Response Agent Data Populator"
		DCOSP_DPFUNC=DCOSP32_SMPopulatorDispatch
		DCOSP_DNAME="Operating System Data Populator"
		DCSECP_DPFUNC=DCSECP32_SMPopulatorDispatch
		DCSECP_DNAME="Security Data Populator"
		DCIFRU_DPFUNC=DCIFRU32_SMPopulatorDispatch
		DCIFRU_DNAME="IPMI Field Replaceable Unit Data Populator"
		${DCECFG} ${CMD_MISC} aliasname=dccoop32 pathfname="${LOC_ISVC_BINPATH}/libdccoop32.so" dispatchfn=${DCCOOP_DPFUNC} displayname="${DCCOOP_DNAME}"
		${DCECFG} ${CMD_MISC} aliasname=dclra32I pathfname="${LOC_ISVC_BINPATH}/libdclra32.so" dispatchfn=${DCLRA_DPFUNC} displayname="${DCLRA_DNAME}"
		${DCECFG} ${CMD_MISC} aliasname=dcosp32 pathfname="${LOC_ISVC_BINPATH}/libdcosp32.so" dispatchfn=${DCOSP_DPFUNC}  displayname="${DCOSP_DNAME}"
		${DCECFG} ${CMD_MISC} aliasname=dcsecp32 pathfname="${LOC_ISVC_BINPATH}/libdcsecp32.so"	dispatchfn=${DCSECP_DPFUNC} displayname="${DCSECP_DNAME}"
		${DCECFG} ${CMD_MISC} aliasname=dcifru32 pathfname="${LOC_ISVC_BINPATH}/libdcifru32.so"	dispatchfn=${DCIFRU_DPFUNC} displayname="${DCIFRU_DNAME}"
		fi

		# srvadmin-cm
		CreateShadowFuncs "srvadmin-cm" "${RPM_INSTALL_PREFIX}" "${FUNCS}"

		# srvadmin-iws
		CreateShadowFuncs "srvadmin-iws" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
		#  expand iws install path in omprv32.ini
		FixPath /etc/omreg.cfg oma/ini/omprv32.ini openmanage.omacore.installpath srvadmin-iws "${RPM_INSTALL_PREFIX}" replace_om_iws_path
	#  create the web server src file with the following content
	echo >${RPM_INSTALL_PREFIX}/iws/bin/linux/omawsd.src
	echo "export PROGRAM_NAME='Secure Port Server'" >>${RPM_INSTALL_PREFIX}/iws/bin/linux/omawsd.src
	echo "export INSTALL_ROOT=${RPM_INSTALL_PREFIX}" >>${RPM_INSTALL_PREFIX}/iws/bin/linux/omawsd.src
	echo "sLibPath=\`grep omalibrarypath ${OMACOREINSTALPATH}/oma/ini/omprv32.ini\`" >>${RPM_INSTALL_PREFIX}/iws/bin/linux/omawsd.src
	echo "sLibPath=\${sLibPath/omalibrarypath=/}" >>${RPM_INSTALL_PREFIX}/iws/bin/linux/omawsd.src
	echo "sLibPath=\${sLibPath//,/:}" >>${RPM_INSTALL_PREFIX}/iws/bin/linux/omawsd.src
	echo "export LD_LIBRARY_PATH=\$LD_LIBRARY_PATH:\$sLibPath" >>${RPM_INSTALL_PREFIX}/iws/bin/linux/omawsd.src

	#  Update omreg.cfg with latest values
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" openmanage.iws.installpath=
	
	# srvadmin-old
	CreateShadowFuncs "srvadmin-old" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	# Expand diags install path in omprv32.ini
	FixPath /etc/omreg.cfg oma/ini/omprv32.ini openmanage.omacore.installpath srvadmin-old "${RPM_INSTALL_PREFIX}" replace_om_diags_path

	# Expand <installdir> tag in lib/DiagnosticEngine.properties
	FILE=${RPM_INSTALL_PREFIX}/oldiags/lib/DiagnosticEngine.properties
	sed "s#<installdir>#${RPM_INSTALL_PREFIX}#g" ${FILE} > ${FILE}.tmp &&
	  cat ${FILE}.tmp > ${FILE}
	rm -f ${FILE}.tmp
	# Expand <installdir> tag in lib/DiagController.properties
	FILE=${RPM_INSTALL_PREFIX}/oldiags/lib/DiagController.properties
	sed "s#<installdir>#${RPM_INSTALL_PREFIX}#g" ${FILE} > ${FILE}.tmp &&
	  cat ${FILE}.tmp > ${FILE}
	rm -f ${FILE}.tmp
	# Expand <installdir> tag in bin/diagctrl.cfg
	FILE=${RPM_INSTALL_PREFIX}/oldiags/bin/diagctrl.cfg
	sed "s#<installdir>#${RPM_INSTALL_PREFIX}#g" ${FILE} > ${FILE}.tmp &&
	  cat ${FILE}.tmp > ${FILE}
	rm -f ${FILE}.tmp
	# Expand <systemid> tag in DiagnosticEngine.properties
	# read the location of utilities from omreg file
	SYSIDCHECKPATH=`GetRegVal /etc/omreg.cfg openmanage.sysidchkutlpath`
	# Check for the system id by getting the path to the system id util,
	# getting the sys id, and grepping it from the sys id list
	SYSID=`${SYSIDCHECKPATH} | grep "System ID" | sed 's#^.*0x##; s#[[:space:]].*$##'`
	FILE=${RPM_INSTALL_PREFIX}/oldiags/bin/diagctrl.cfg
	sed "s#<systemid>#$SYSID#g" ${FILE} > ${FILE}.tmp &&
	  cat ${FILE}.tmp > ${FILE}
	rm -f ${FILE}.tmp
	# Grep for the MachineID
	DellMachineID=`GetRegVal ${RPM_INSTALL_PREFIX}/oldiags/bin/diagctrl.cfg	MachineId`
	# OMSA CLI Registration
	OMINSTPATHVALUE=`GetRegVal /etc/omreg.cfg openmanage.omilcore.installpath`
	CLIREGUTIL=${OMINSTPATHVALUE}/omil/tools/cli_ini_modifier.sh
	$CLIREGUTIL ${OMINSTPATHVALUE}/omil/supportfiles/ini/old.ini add

	# srvadmin-storage
	CreateShadowFuncs "srvadmin-storage" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	#  Update omreg.cfg with latest values
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" openmanage.storage.installpath=
	#  get omacore install path from omreg.cfg and
	#  expand storage install path in omprv32.ini
	FixPath /etc/omreg.cfg oma/ini/omprv32.ini openmanage.omacore.installpath srvadmin-storage "${RPM_INSTALL_PREFIX}" replace_om_storage_path
	#  CLI Registration
	OMINSTPATHVALUE=`GetRegVal /etc/omreg.cfg openmanage.omilcore.installpath`
	CLIREGUTIL=${OMINSTPATHVALUE}/omil/tools/cli_ini_modifier.sh
	$CLIREGUTIL ${OMINSTPATHVALUE}/omil/supportfiles/ini/storage.ini add
	# Register with DataEngine
	DCECFG=`GetRegVal /etc/omreg.cfg OMDataEngine.configtool`
	if [ -z "$DCECFG" -o ! -x "$DCECFG" ]; then
		echo "WARNING: srvadmin-storage configuration not performed;"
		echo "'/etc/omreg.cfg' is missing or damaged."
	else
		# configure Storage Services MIB Implementor
		$DCECFG command=addmpialias \
				update=TRUE \
				startuptype=automatic \
				aliasname=ssimp \
				pathfname="${RPM_INSTALL_PREFIX}/sm/libssimp32.so" \
				dispatchfn=OMSSIMP32_SMMIBImplementerDispatch \
				displayname="OMSM_SNMP_IMPLEMENTER"
		# configure Storage Services OM Data Populator
		$DCECFG command=addpopalias \
				update=TRUE \
				startuptype=automatic \
				aliasname=ral32 \
				pathfname="${RPM_INSTALL_PREFIX}/sm/libral32.so" \
				dispatchfn=RAL_SMPopulatorDispatch \
				displayname="OMSM_POPULATOR_IMPLEMENTER"
		# configure event logging
		$DCECFG command=addepialias \
				update=TRUE \
				startuptype=automatic \
				aliasname=dcsipe32 \
				pathfname="${RPM_INSTALL_PREFIX}/sm/libdcsipe32.so" \
				dispatchfn=DCSIPE32_SMEventImplementerDispatch \
				displayname="OMSM_EVENT_IMPLEMENTER"
	fi
	#  Create /etc/dellinst.cfg
	#  TODO: this should be removed and appropriate code
	#        should be added to read the install location from
	#        /etc/omreg.cfg
	touch /etc/dellinst.cfg
	chmod 644 /etc/dellinst.cfg
	echo "omss.installpath=${RPM_INSTALL_PREFIX}" >> /etc/dellinst.cfg
	chmod 644 /etc/dellinst.cfg

	# srvadmin-omhip
	CreateShadowFuncs "srvadmin-omhip" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	#  get omacore install path from omreg.cfg and
	#  expand omhip install path in omprv32.ini
	FixPath /etc/omreg.cfg oma/ini/omprv32.ini openmanage.omacore.installpath srvadmin-omhip "${RPM_INSTALL_PREFIX}" replace_om_hip_path
	#  OMSA CLI Registration
	OMINSTPATHVALUE=`GetRegVal /etc/omreg.cfg openmanage.omilcore.installpath`
	CLIREGUTIL=${OMINSTPATHVALUE}/omil/tools/cli_ini_modifier.sh
	$CLIREGUTIL ${OMINSTPATHVALUE}/omil/supportfiles/ini/omhip.ini add
	#  Update omreg.cfg with latest values
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" openmanage.omhip.installpath=

	# srvadmin-storage
	CreateShadowFuncs "srvadmin-storage" "${RPM_INSTALL_PREFIX}" "${FUNCS}"
	#  Update omreg.cfg with latest values
	UpdateRegPair /etc/omreg.cfg "${RPM_INSTALL_PREFIX}" openmanage.storage.installpath=
	#  get omacore install path from omreg.cfg and
	#  expand storage install path in omprv32.ini
	FixPath /etc/omreg.cfg oma/ini/omprv32.ini openmanage.omacore.installpath srvadmin-storage "${RPM_INSTALL_PREFIX}" replace_om_storage_path
	OMINSTPATHVALUE=`GetRegVal /etc/omreg.cfg openmanage.omilcore.installpath`
	CLIREGUTIL=${OMINSTPATHVALUE}/omil/tools/cli_ini_modifier.sh
	$CLIREGUTIL ${OMINSTPATHVALUE}/omil/supportfiles/ini/storage.ini add
	# Register with DataEngine
	DCECFG=`GetRegVal /etc/omreg.cfg OMDataEngine.configtool`
	if [ -z "$DCECFG" -o ! -x "$DCECFG" ]; then
		echo "WARNING: srvadmin-storage configuration not performed;"
		echo "'/etc/omreg.cfg' is missing or damaged."
	else
		# configure Storage Services MIB Implementor
		$DCECFG command=addmpialias \
				update=TRUE \
				startuptype=automatic \
				aliasname=ssimp \
				pathfname="${RPM_INSTALL_PREFIX}/sm/libssimp32.so" \
				dispatchfn=OMSSIMP32_SMMIBImplementerDispatch \
				displayname="OMSM_SNMP_IMPLEMENTER"

		# configure Storage Services OM Data Populator
		$DCECFG command=addpopalias \
				update=TRUE \
				startuptype=automatic \
				aliasname=ral32 \
				pathfname="${RPM_INSTALL_PREFIX}/sm/libral32.so" \
				dispatchfn=RAL_SMPopulatorDispatch \
				displayname="OMSM_POPULATOR_IMPLEMENTER"

		# configure event logging
		$DCECFG command=addepialias \
				update=TRUE \
				startuptype=automatic \
				aliasname=dcsipe32 \
				pathfname="${RPM_INSTALL_PREFIX}/sm/libdcsipe32.so" \
				dispatchfn=DCSIPE32_SMEventImplementerDispatch \
				displayname="OMSM_EVENT_IMPLEMENTER"
	fi

	einfo "Done."
}
