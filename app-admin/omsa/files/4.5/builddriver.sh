#!/bin/sh

# Wouter Coppens <wouter.coppens@dedigate.com>

if [ "$1" = "" ]; then
  echo "USAGE: `basename $0` <kernelsrcdir>"
  exit 1
fi
KERNELDIR=$1

MODULE_LIST="dcdbas dcdesm dcdipm dcdtvm"

# Let's find the install path
if [ ! -f /etc/omreg.cfg ]; then
	echo "ERROR: File /etc/omreg.cfg is missing"
	exit 1
fi
HAPI_INSTALL_PATH=`grep hapi.installpath /etc/omreg.cfg | cut -d = -f2`
if [ ! -d ${HAPI_INSTALL_PATH} ]; then
	echo "ERROR: hapi install path cannot be found"
	exit 1
fi

# check that gcc is new enough
GCC_VER=`gcc -dumpversion 2>/dev/null`
if [ $? != 0 ]; then
	echo "ERROR: GCC not found"
	exit 1
fi
# Use gcc version to determine which precompiled objects to use
GCC_MAJORVER=`echo ${GCC_VER} | sed "s/\..*//"`
GCC_MINORVER=`echo ${GCC_VER} | sed "s/.\.//" | sed "s/\..*//"`
if [ ${GCC_MAJORVER} -lt 3 ]; then
	DKS_OBJ_SUBDIR="objgcc296"
elif [ ${GCC_MAJORVER} -eq 3 ] && [ ${GCC_MINORVER} -lt 3 ]; then
	DKS_OBJ_SUBDIR="objgcc32"
else 
	DKS_OBJ_SUBDIR="objgcc34"
fi

# TODO: find out proc_arch:
# For now it only works for x86
PROC_ARCH="x86"
if [ -f "${HAPI_INSTALL_PATH}/bin/dchcfg64" ]; then
	BIT_SIZE="64"
else 
	BIT_SIZE="32"
fi


# Kernel settings
if [ ! -d $KERNELDIR ]; then
  echo "ERROR: ${KERNELDIR} does not exist or is not directory"
  exit 1
fi
if [ -f $KERNELDIR/Makefile ]; then
	KERNEL_VERSION=`head -n3 $KERNELDIR/Makefile | grep VERSION | cut -d = -f 2`
	KERNEL_PATCHLEVEL=`head -n3 $KERNELDIR/Makefile | grep PATCHLEVEL | cut -d = -f 2`
	KERNEL_SUBLEVEL=`head -n3 $KERNELDIR/Makefile | grep SUBLEVEL | cut -d = -f 2`
	if [ ${KERNEL_VERSION} -gt 2 ]; then
		LOC_KERNEL_IS_GT_2_4=1
	elif [ ${KERNEL_VERSION} -eq 2 ] && [ ${KERNEL_PATCHLEVEL} -gt 4 ]; then
		LOC_KERNEL_IS_GT_2_4=1
	else 
		echo "ERROR: Only kernels starting at 2.6 are supported"
		exit 1
	fi
fi
if [ ! -f $KERNELDIR/.config ]; then
  echo "ERROR: Kernel in ${KERNELDIR} does not have .config"
  exit 1
fi
# check that given kernel has CONFIG_REGPARM enabled
RETVAL=`grep -q CONFIG_REGPARM $KERNELDIR/.config`
if [ $? -eq 0 ]; then
    echo -n "CONFIG_REGPARM found: "
    RETVAL=`grep -q CONFIG_REGPARM=y $KERNELDIR/.config`
    if [ $? -eq 0 ]; then
        echo "enabled, compiling drivers."
    else
        echo "disabled, not compiling drivers."
        exit 0
    fi
else
    echo "ERROR: CONFIG_REGPARM option not found in .config?"
    exit 1
fi

# Set up DKS path variables HAPI_INSTALL_PATH
DKS_INSTALL_BASE_DIR="${HAPI_INSTALL_PATH}/drivers"
DKS_VARDATA_BASE_DIR="${DKS_INSTALL_BASE_DIR}/${PROC_ARCH}/dks"

# compile the drivers

mkdir -p /tmp/buildtmp.$$ 

# Prepare common
cp -a ${DKS_VARDATA_BASE_DIR}/common /tmp/buildtmp.$$
# if kernel >= 2.6.14, we need to patch the common files
if [ ${KERNEL_VERSION} -eq 2 ] && [ ${KERNEL_PATCHLEVEL} -eq 6 ] && [ ${KERNEL_SUBLEVEL} -gt 13 ]; then
	cd /tmp/buildtmp.$$
	patch -p0 < ${HAPI_INSTALL_PATH}/bin/dcddks-2.6.14.patch
fi
for MODULE in ${MODULE_LIST}; do
    rm -rf /tmp/buildtmp.$$/${MODULE}
    mkdir -p /tmp/buildtmp.$$/${MODULE}
    # Make links to common files
    cd /tmp/buildtmp.$$/common
    for FILE in `ls *.c *.h`; do
    	ln -sf /tmp/buildtmp.$$/common/${FILE} /tmp/buildtmp.$$/${MODULE}/${FILE}
    done
    #Copy file to the DKS build directory
    cp -fp ${DKS_VARDATA_BASE_DIR}/${MODULE}/*.mak /tmp/buildtmp.$$/${MODULE}
    cp -fp ${DKS_VARDATA_BASE_DIR}/${MODULE}/${DKS_OBJ_SUBDIR}/* /tmp/buildtmp.$$/${MODULE}
    mv -f /tmp/buildtmp.$$/${MODULE}/*.mak /tmp/buildtmp.$$/${MODULE}/Makefile
    # Start compiling
    (
        cd /tmp/buildtmp.$$/${MODULE}
        make -C ${KERNELDIR} SUBDIRS=${PWD} KERNEL_IS_GT_2_4=${LOC_KERNEL_IS_GT_2_4} modules
        make -C ${KERNELDIR} SUBDIRS=${PWD} KERNEL_IS_GT_2_4=1 modules_install
    )
done

rm -rf buildtmp.$$
