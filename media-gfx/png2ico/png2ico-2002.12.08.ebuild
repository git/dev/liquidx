# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-gfx/pngtoico/pngtoico-1.0.ebuild,v 1.7 2005/04/09 17:36:34 blubb Exp $

inherit eutils

MY_PV=2002-12-08
DESCRIPTION="PNG to Icon Convertor"
HOMEPAGE="http://www.winterdrache.de/freeware/png2ico/"
SRC_URI="http://www.winterdrache.de/freeware/png2ico/data/${PN}-src-${MY_PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="media-libs/libpng"
S=${WORKDIR}/${PN}

src_compile() {
	emake || die
}

src_install() {
	dobin png2ico 
	dodoc doc/png2ico.txt README README.unix
	doman doc/png2ico.1
}
