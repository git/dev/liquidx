# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-wireless/bluez-libs/bluez-libs-2.25.ebuild,v 1.4 2006/07/06 06:29:52 corsair Exp $

inherit eutils multilib

DESCRIPTION="Bluetooth Userspace Libraries"
HOMEPAGE="http://bluez.sourceforge.net/"
SRC_URI="http://bluez.sourceforge.net/download/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86"

IUSE=""
DEPEND=">=net-wireless/bluez-libs-3.4
	>=net-wireless/bluez-utils-3.4
	>=dev-libs/glib-2.0
	>=x11-libs/gtk+-2.8
	>=sys-apps/dbus-0.60
	>=x11-libs/libnotify-0.3.2"

src_install() {
	make DESTDIR="${D}" install || die "make install failed"
	dodoc AUTHORS README NEWS
}
