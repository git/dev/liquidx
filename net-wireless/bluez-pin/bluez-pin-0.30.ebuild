# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils gnome2

DESCRIPTION="Bluetooth PIN input via DBUS"
HOMEPAGE="http://www.handhelds.org/"
SRC_URI="http://www.handhelds.org/download/projects/gpe/source/${P}.tar.bz2"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=x11-libs/gtk+-2.0
    >=sys-apps/dbus-0.61
    >=net-wireless/bluez-libs-3.0
    >=gnome-base/gconf-2.0
    >=gnome-base/libglade-2.0"

S=${WORKDIR}/${P}

src_unpack() {
	unpack ${A}
	cd ${S}
	epatch ${FILESDIR}/${PV}-debian.patch
}