# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-wireless/bluez-utils/bluez-utils-2.25-r1.ebuild,v 1.4 2006/06/26 13:59:11 gustavoz Exp $

inherit eutils

DESCRIPTION="Bluetooth Tools and System Daemons for Linux"
HOMEPAGE="http://bluez.sourceforge.net/"
SRC_URI="http://bluez.sourceforge.net/download/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~hppa ~ppc ~sparc ~x86"

IUSE="alsa cups fuse udev"

RDEPEND="!net-wireless/bluez-pan
		>=net-wireless/bluez-libs-3.7
		>=sys-apps/dbus-0.31
		dev-libs/libusb
		>=dev-libs/openobex-1.2
		fuse? ( sys-fs/fuse )
		alsa? ( >=media-libs/alsa-lib-1.0.9 )
		cups? ( net-print/cups )
		udev? ( sys-fs/udev )"

DEPEND="sys-devel/bison
		sys-devel/flex
		>=sys-apps/sed-4
		${RDEPEND}"

pkg_setup() {
	ewarn "bluez-utils-3.x uses a new DBUS Passkey API to do PIN"
	ewarn "authentication. The new GNOME Applet that handles PIN"
	ewarn "entry is now in 'net-wireless/bluez-gnome'. This will"
	ewarn "installed to be launched on login."
	ewarn ""
	ewarn "If you would like to do PIN authentication without a"
	ewarn "GUI, you can run this DBUS agent to provide a default"
	ewarn "pin as hcid now ignores it's own configuration file."
	ewarn ""
	ewarn "/usr/bin/passkey-agent --default <pin number>"
	ewarn ""
	ewarn "bluez-utils-3.x may ignore pscan/iscan configure options in"
	ewarn "hciconfig. Instead, you must use DBUS to re-enable the older"
	ewarn "insecure behaviour:"
	ewarn ""
	ewarn "dbus-send --system --type=method-call --print-reply \\"
	ewarn "--dest=org.bluez \\"
	ewarn "  /org/bluez/hci0 org.bluez.Adapter.SetDiscoverableTimeout uint32:0"
	ewarn ""
	ewarn "dbus-send --system --type=method-call --print-reply \\"
	ewarn "--dest=org.bluez \\"
	ewarn "  /org/bluez/hci0 org.bluez.Adapter.SetMode string:discoverable"
	ewarn ""
}

src_unpack() {
	unpack ${A}

	sed -i \
		-e "s:^HIDD_ENABLE=.*:HIDD_ENABLE=false:" \
		-e "s:^HID2HCI_ENABLE=.*:HID2HCI_ENABLE=false:" \
		-e "s:^\(# Start dund.*\)$:\1\n# If you want to use dund, you must install\: net-dialup/ppp .:" \
		${S}/scripts/bluetooth.default

	sed -i \
		-e "s:security .*;:security user;:" \
		${S}/hcid/hcid.conf

	# fix cups-1.2
	if use cups; then
		sed -i -e "s:\$(libdir)/cups:$(cups-config --serverbin):" ${S}/cups/Makefile.{am,in}
	fi
}

src_compile() {
	econf \
		$(use_enable alsa) \
		$(use_enable cups) \
		$(use_enable fuse) \
		--enable-dbus \
		--enable-avctrl \
		--enable-bcm203x \
		--enable-dfutool \
		--enable-hid2hci \
		--enable-obex \
		--disable-initscripts \
		--localstatedir=/var \
		|| die "econf failed"

	emake || die "emake failed"
}

src_install() {
	make DESTDIR="${D}" install || die "make install failed"

	dodoc AUTHORS ChangeLog README

	# optional bluetooth utils
	dosbin tools/hcisecfilter tools/ppporc 
	dobin hcid/passkey-agent

	newinitd ${FILESDIR}/${PN}-2.25-init.d bluetooth
	newconfd ${S}/scripts/bluetooth.default bluetooth

	# bug #84431
	if use udev; then
		insinto /etc/udev/rules.d/
		newins ${FILESDIR}/${PN}-2.24-udev.rules 70-bluetooth.rules

		exeinto /lib/udev/
		newexe ${FILESDIR}/${PN}-2.24-udev.script bluetooth.sh
	fi

}

pkg_postinst() {
	einfo
	einfo "A startup script has been installed in /etc/init.d/bluetooth."
	einfo

	if use udev; then
		einfo "You need to run 'udevstart' or reboot for the udev rules"
		einfo "to take effect."
		einfo
	fi
}
