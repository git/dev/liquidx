# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A program that creates emergency boot disks/CDs using your kernel, tools and modules."
HOMEPAGE="http://www.mondorescue.org"
SRC_URI="ftp://ftp.mondorescue.org/src/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86 -*"
IUSE=""

DEPEND="virtual/libc"

RDEPEND=">=app-arch/bzip2-0.9
		sys-libs/ncurses
		sys-devel/binutils
		sys-fs/dosfstools
		sys-apps/gawk"

src_install() {
	export PREFIX="${D}/usr"
	export CONFDIR="${D}/etc"
	export DONT_RELINK=1
	export RPMBUILDMINDI="true"
	"${WORKDIR}"/"${P}"/install.sh
}
