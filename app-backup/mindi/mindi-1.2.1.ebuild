# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A program that creates emergency boot disks/CDs using your kernel, tools and modules."
HOMEPAGE="http://www.mondorescue.org"
SRC_URI="ftp://ftp.mondorescue.org/debian/3.1/${P/-/_}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86 amd64 -*"
IUSE=""

DEPEND="virtual/libc"

RDEPEND=">=app-arch/bzip2-0.9
		sys-libs/ncurses
		sys-devel/binutils
		sys-fs/dosfstools
		sys-apps/gawk
		>=app-backup/mindi-busybox-1.2.2"

S="${WORKDIR}/mindi-stable"

src_install() {
	export HEAD="${D}"
	export PREFIX="/usr"
	export CONFDIR="/etc"
	export MANDIR="${PREFIX}/share/man"
	export LIBDIR="${PREFIX}/lib"
	export DOCDIR="${PREFIX}/share/doc"
	export DONT_RELINK=1
	export RPMBUILDMINDI="true"
	${S}/install.sh
}
