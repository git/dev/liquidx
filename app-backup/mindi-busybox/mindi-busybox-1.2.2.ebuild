# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MY_P=${PN}_${PV}
DESCRIPTION="Busybox binaries for Mindi rootfs."
HOMEPAGE="http://www.mondorescue.org"
SRC_URI="ftp://ftp.mondorescue.org/debian/3.1/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

DEPEND="virtual/libc"

S="${WORKDIR}/mindi-busybox-stable"

src_compile() {
    cd ${S}
    make oldconfig
    make busybox
}

src_install() {
    dodir /usr/lib/mindi/rootfs
    cd ${S}
    make install PREFIX=${D}/usr/lib/mindi/rootfs
}
