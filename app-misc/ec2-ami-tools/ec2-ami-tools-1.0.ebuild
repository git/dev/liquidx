# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-themes/redhat-artwork/redhat-artwork-0.243-r1.ebuild,v 1.8 2006/11/01 20:18:10 kloeri Exp $

inherit eutils rpm

DESCRIPTION="Amazon EC2 AMI Tools"
HOMEPAGE="http://developer.amazonwebservices.com/connect/entry.jspa?entryID=368&ref=featured"
SRC_URI="http://s3.amazonaws.com/ec2-downloads/ec2-ami-tools.noarch.rpm"
LICENSE="as-is"

SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="dev-lang/ruby"
DEPEND="${RDEPEND}"

S=${WORKDIR}

src_install () {
	cd ${S}

	insinto /usr/lib/ruby/site_ruby
	doins -r ${S}/usr/lib/site_ruby/*

	insinto /etc
	doins -r ${S}/etc/*

	exeinto /usr/lib/aes/amiutil
	doexe ${S}/usr/local/aes/amiutil/*

	insinto /usr/bin
	for exe in ${S}/usr/local/bin/*; do
		if [ -h "${exe}" ]; then
			target="$(basename `readlink ${exe}`)"
			base="$(basename ${exe})"
			ln -s /usr/lib/aes/amiutil/$target ${D}/usr/bin/${base}
        fi
	done

	sed -i -e 's:site_ruby:ruby/site_ruby:' ${D}/usr/lib/aes/amiutil/*
}