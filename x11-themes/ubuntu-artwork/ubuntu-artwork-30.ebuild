# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/x11-themes/gtk-engines-industrial/gtk-engines-industrial-0.2.36.ebuild,v 1.6 2005/09/07 13:39:05 gustavoz Exp $

inherit eutils

MY_P=${PN}_${PV}
DESCRIPTION="Ubuntu Artwork"
HOMEPAGE="http://packages.ubuntu.com/edgy/gnome/ubuntu-artwork"
SRC_URI="http://archive.ubuntu.com/ubuntu/pool/main/u/${PN}/${MY_P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86"

RDEPEND="x11-themes/gtk-engines-ubuntulooks"

DEPEND=">=gnome-base/gnome-common-2
	${RDEPEND}"


src_compile() {
	./autogen.sh
	econf 
	emake || die "Compilation failed"
}

src_install() {
	make DESTDIR="${D}" install || die "Installation failed"
	dodoc ChangeLog README
}
