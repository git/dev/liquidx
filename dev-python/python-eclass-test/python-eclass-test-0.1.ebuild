# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/cjkcodecs/cjkcodecs-1.1.1.ebuild,v 1.4 2005/03/04 21:31:57 kito Exp $

inherit python

DESCRIPTION="Dummy Ebuild to test python"

LICENSE="as-is"
SLOT="0"
KEYWORDS="x86"
IUSE=""

DEPEND="dev-lang/python"

pkg_setup() {
	einfo "Testing python_extract_version():"
	__python_eclass_test

	python_version
	einfo "I think the current version of python is: ${PYVER}"

	python_disable_pyc
	if [ "x$PYTHON_DONTCOMPILE" = "x1" ]; then
		einfo "Disabling pyc generation works!"
	else
		eerror "Disabling pyc generation doesn't work!"
    fi

	python_enable_pyc
	if [ "x$PYTHON_DONTCOMPILE" = "x" ]; then
		einfo "Enabling pyc generation works!"
	else
		eerror "Enabling pyc generation doesn't work!"
	fi
}



