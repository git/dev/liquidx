# Copyright 1999-2004 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit distutils
MY_PN="OpenGLContext"
MY_PV="2.0.0c1" # change when version has an a1 or similar extension

DESCRIPTION="Testing and demonstration contexts for PyOpenGL"
HOMEPAGE="http://pyopengl.sourceforge.net/context/"
SRC_URI="mirror://sourceforge/pyopengl/${MY_PN}-${MY_PV}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~x86 ~amd64" 

IUSE=""
RDEPEND="virtual/python
	dev-python/ttfquery
	dev-python/imaging
	dev-python/numeric
	dev-python/pydispatcher
	>=dev-python/pyopengl-2.0.1.09"
DEPEND="virtual/python"

S="${WORKDIR}/${MY_PN}-${MY_PV}"
