# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/pygoogle/pygoogle-0.6.ebuild,v 1.5 2006/04/01 15:21:23 agriffis Exp $

inherit distutils

DESCRIPTION="Python Port Forwarder"
SRC_URI="ftp://ftp.tummy.com/pub/tummy/pynetd/${P}.tar.gz"
HOMEPAGE="ftp://ftp.tummy.com/pub/tummy/pynetd/README"

IUSE=""
SLOT="0"
LICENSE="MPL"
KEYWORDS="~x86"

DEPEND="dev-lang/python"

src_compile() {
	return
}

src_install() {
	cd ${S}
	dobin pynetd
	dodoc README WHATSNEW
	insinto /etc
	newins pynetd.conf-dist pynetd.conf
}

