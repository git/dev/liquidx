# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/pygoogle/pygoogle-0.6.ebuild,v 1.5 2006/04/01 15:21:23 agriffis Exp $

inherit distutils

DESCRIPTION="Python Implementation of Netcat"
SRC_URI="ftp://ftp.tummy.com/pub/tummy/pynetcat/${P}.tar.gz"
HOMEPAGE="ftp://ftp.tummy.com/pub/tummy/pynetcat/README"

IUSE=""
SLOT="0"
LICENSE="PYTHON"
KEYWORDS="~x86"

DEPEND="dev-lang/python"

src_compile() {
	return
}

src_install() {
	cd ${S}
	dobin py*
	dodoc README LICENSE
}