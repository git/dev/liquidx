# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/cjkcodecs/cjkcodecs-1.1.1.ebuild,v 1.4 2005/03/04 21:31:57 kito Exp $

inherit distutils

DESCRIPTION="Python interface to the mcrypt library, which provides a uniform interface to several symmetric encryption algorithms."
HOMEPAGE="http://sourceforge.net/projects/python-mcrypt/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="dev-lang/python
dev-libs/libmcrypt"

