# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/pygoogle/pygoogle-0.6.ebuild,v 1.5 2006/04/01 15:21:23 agriffis Exp $

inherit distutils versionator

DEB_VER=$(replace_version_separator 3 '-')
DESCRIPTION="Python Interface to PAM"
SRC_URI="mirror://debian/pool/main/p/python-pam/${PN}_${DEB_VER}.tar.gz"
HOMEPAGE="http://packages.debian.org/unstable/python/python-pam"

IUSE=""
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86"

DEPEND="dev-lang/python sys-libs/pam"
S="${WORKDIR}/${PN}-0.4.2"

src_install() {
	distutils_src_install
	insinto /usr/share/doc/${PF}/examples
	doins ${S}/examples/*
}