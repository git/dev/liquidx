# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/cjkcodecs/cjkcodecs-1.1.1.ebuild,v 1.4 2005/03/04 21:31:57 kito Exp $

inherit distutils

REV=${PV/0.0_pre/}
DESCRIPTION="Python implementation of the WSGI and FastCGI interfaces"
HOMEPAGE="http://www.saddi.com/software/flup/"
SRC_URI="http://www.saddi.com/software/flup/dist/${PN}-r${REV}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=dev-lang/python-2.4"
S=${WORKDIR}/${PN}-r${REV}
