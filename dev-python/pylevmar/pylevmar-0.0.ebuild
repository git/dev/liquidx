# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/pygoogle/pygoogle-0.6.ebuild,v 1.4 2005/07/02 16:48:16 fserb Exp $

inherit eutils distutils subversion

DESCRIPTION="JSON reader/writer for python."
HOMEPAGE="http://projects.liquidx.net/python/browser/pylevmar/"
ESVN_REPO_URI="http://svn.liquidx.net/svn/python/pylevmar/trunk"
ESVN_PROJECT="pylevmar"

IUSE=""
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86"

DEPEND="sci-libs/levmar dev-lang/python"

pkg_setup() {
	if ! built_with_use sci-libs/levmar lapack; then
		eerror "sci-libs/levmar needs to be built with USE=\"lapack\""
	fi
}
	