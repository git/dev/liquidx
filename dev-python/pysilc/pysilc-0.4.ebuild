# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/feedparser/feedparser-4.1.ebuild,v 1.6 2006/07/16 13:58:13 liquidx Exp $

inherit distutils

DESCRIPTION="Python bindings to SILC Toolkit"
HOMEPAGE="http://www.liquidx.net/pysilc/"
SRC_URI="http://download.berlios.de/python-silc/${P}.tar.bz2"
LICENSE="BSD"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
DEPEND=">=dev-lang/python-2.4
	net-im/silc-toolkit"

src_install() {
	distutils_src_install
	insinto /usr/share/doc/${P}
	doins -r supybot examples
}