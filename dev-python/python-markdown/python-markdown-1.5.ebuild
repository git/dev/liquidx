# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/pygame/pygame-1.7.1.ebuild,v 1.2 2005/08/26 16:04:58 agriffis Exp $

inherit versionator multilib python

MY_PV=$(replace_version_separator 1 '-')
DESCRIPTION="Python implementation of the Markdown syntax"
HOMEPAGE="http://www.freewisdom.org/projects/python-markdown/"
SRC_URI="mirror://sourceforge/python-markdown/markdown-${MY_PV}.py"

LICENSE="|| ( GPL-2 BSD )"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="dev-lang/python"
S=${WORKDIR}

src_unpack() {
	cp ${DISTDIR}/${A} ${WORKDIR}
}

src_compile() {
	return
}

src_install() {
	python_version
	insinto /usr/$(get_libdir)/python${PYVER}/site-packages
	newins ${WORKDIR}/${A} markdown.py
}
