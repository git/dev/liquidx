# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# http://bugs.gentoo.org/show_bug.cgi?id=39457

inherit distutils

DESCRIPTION="Python wrapper around RXP, a very fast validating XML parser"
SRC_URI="http://dev.gentoo.org/~liquidx/snapshots/${P}.tgz"
HOMEPAGE="http://www.reportlab.org/pyrxp.html"

DEPEND=">dev-lang/python-2.2"
IUSE=""
SLOT="0"
LICENSE="LGPL-2.1"
KEYWORDS="~x86"

S=${WORKDIR}/pyRXP-${PV/_pre/-}/pyRXP

src_install() {
	distutils_src_install

	dodoc docs/* README
	insinto /usr/share/doc/${PF}
	doins -r examples
	doins -r test
}
