# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/pygoogle/pygoogle-0.6.ebuild,v 1.5 2006/04/01 15:21:23 agriffis Exp $

inherit distutils

DESCRIPTION="Python implementation of the Sender Permitted From (SPF) protocol"
SRC_URI="http://www.wayforward.net/spf/${P}.tar.gz"
HOMEPAGE="http://www.wayforward.net/spf/"

IUSE=""
SLOT="0"
LICENSE="PYTHON"
KEYWORDS="~x86"

DEPEND="dev-lang/python"

